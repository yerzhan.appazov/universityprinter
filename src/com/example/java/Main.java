package com.example.java;

public class Main {

    public static void main(String[] args) {
        User student = new Student();
        student.authorize();
        Permission permission = new Permission(student);
        Settings settings = new Settings();
        Printer printer = new Printer(permission, student, settings);
        printer.print();
        printer.scan();
        System.out.println();

        User guest = new Guest();
        guest.authorize();
        permission = new Permission(guest);
        printer = new Printer(permission, guest, settings);
        printer.scan();
        printer.print();
    }
}
