package com.example.java;

public class Printer {
    Permission permissions;
    Settings settings;
    User user;

    public Printer(Permission permissions, User user, Settings settings) {
        this.permissions = permissions;
        this.user = user;
        this.settings = settings;
    }


    public void print() {
        if (permissions.getPermission() > 0) {
            System.out.println("Printed: " + user.getDocumentToPrint() + "\nUsing these " +
                    settings.toString());
            user.papersAllowedLeft--;
        } else {
            System.err.println("Sorry, but you are not allowed to print. " +
                    "Paper limit is reached or you are entering as a Guest");
        }
    }

    public void scan() {
        System.out.println("Scanned: " + user.getDocumentToScan());
    }
}
