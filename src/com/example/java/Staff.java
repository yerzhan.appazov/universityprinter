package com.example.java;

public class Staff extends User{
    @Override
    public void authorize() {
        userType = "Staff";
    }

    public String getDocumentToPrint() {
        return "Some Document from staff send by email, flash card or any other way";
    }

    public String getDocumentToScan() {
        return "Some Document from Staff put to the scanning part of the printer";
    }
}
