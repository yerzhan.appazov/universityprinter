package com.example.java;

import java.util.HashMap;
import java.util.Map;

public class Settings {
    private String orientation;
    private boolean coloured;
    private Map<Integer, Integer> size;
    private int numberOfCopies;

    public Settings(String orientation, boolean coloured, Map<Integer, Integer> size, int numberOfCopies) {
        this.orientation = orientation;
        this.coloured = coloured;
        this.size = size;
        this.numberOfCopies = numberOfCopies;
    }

    public Settings() {
        orientation = "book";
        coloured = false;
        Map<Integer, Integer> defaultSize = new HashMap<>();
        defaultSize.put(1920, 1080);
        size = defaultSize;
        numberOfCopies = 1;
    }

    @Override
    public String toString() {
        return "Settings{" +
                "orientation='" + orientation + '\'' +
                ", coloured=" + coloured +
                ", size=" + size +
                ", numberOfCopies=" + numberOfCopies +
                '}';
    }
}
