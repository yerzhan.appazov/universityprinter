package com.example.java;

public class Student extends User{

    @Override
    public void authorize() {
        userType = "Student";
    }
    public String getDocumentToPrint() {
        return "Some Document from Student send by email, flash card or any other way";
    }
    public String getDocumentToScan() {
        return "Some Document from Student put to the scanning part of the printer";
    }
}
