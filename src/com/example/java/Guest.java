package com.example.java;

public class Guest extends User{
    @Override
    public void authorize() {
        userType = "Guest";
    }

    @Override
    public String getDocumentToPrint() {
        return "Some Document from Guest send by email, flash card or any other way";
    }

    public String getDocumentToScan() {
        return "Some Document from Guest put to the scanning part of the printer";
    }
}
