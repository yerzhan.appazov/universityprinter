package com.example.java;

public abstract class User {
    String userType = "default";
    int papersAllowedLeft;
    public abstract void authorize();
    public abstract String getDocumentToPrint();
    public abstract String getDocumentToScan();
}