package com.example.java;

public class Permission {
    public User user;
    private static final int papersAllowedForStudents = 5;
    private static final int papersAllowedForStaff = 99;
    private static final int papersAllowedForGuests = 0;

    public Permission(User user) {
        this.user = user;
    }

    public int getPermission() {
        switch (user.userType) {
            case "Staff":
                user.papersAllowedLeft = papersAllowedForStaff;
                return papersAllowedForStaff;
            case "Student":
                user.papersAllowedLeft = papersAllowedForStudents;
                return papersAllowedForStudents;
            default:
                user.papersAllowedLeft = papersAllowedForGuests;
                return papersAllowedForGuests;
        }
    }


}
